//
//  AppDelegate.h
//  Desafio iOS
//
//  Created by Camila Ribeiro Rodrigues on 13/01/16.
//  Copyright © 2016 Camila Ribeiro Rodrigues. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

