//
//  main.m
//  Desafio iOS
//
//  Created by Camila Ribeiro Rodrigues on 13/01/16.
//  Copyright © 2016 Camila Ribeiro Rodrigues. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
